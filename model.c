void getprimelist(int numberOfPrime, int primelist[])
{
    int i, j, k = 1, flag =0;
    primelist[0] = 2;
    for (i = 3; k < numberOfPrime; i++)
    {
        flag = 0;
        for (j = 2 ; j <= i / 2 ; j++ )
        {
            if (i % j == 0)
            {
                flag = 1;
                break;
            }
        }
        if (flag == 0)
        {
            primelist[k] = i;
            k++;
        }
    }
}

void     getpiles(int index[], int numberOfPrime, int sortedlist[], int numberOfPlates, int primelist[], int list[])
{
    int i, j, count = 0, update = 0;
    for(i = 0; i < numberOfPrime; i++)
    {
        for(j = 0; j < numberOfPlates; j++)
        {
            if(list[j] % primelist[i] == 0)
            {
                index[i] = count++;
                sortedlist[update] = list[j];
                update++;
                for(int k = j; k < numberOfPlates; k++)
                {
                    list[k] = list[k + 1];
                }
                numberOfPlates--;
                j--;
            }
        }
        count = 0;
    }
    for(i = 0; i < numberOfPlates; i++)
    {
        sortedlist[update] = list[i];
        update++;
    }
}

